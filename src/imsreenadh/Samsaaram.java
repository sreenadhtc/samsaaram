package imsreenadh;

import edu.cmu.sphinx.api.Configuration;
import edu.cmu.sphinx.api.LiveSpeechRecognizer;
import edu.cmu.sphinx.api.SpeechResult;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class Samsaaram extends JDialog {
    private JPanel contentPane;
    private JButton buttonStart;
    private JButton buttonCancel;
    private JLabel showResult;

    public Samsaaram() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonStart);
        setMinimumSize(new Dimension(400, 350));
        buttonStart.addActionListener(e -> {
            try {
                onOK();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        });

        buttonCancel.addActionListener(e -> onCancel());

// call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

// call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(e -> onCancel(), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onOK() throws Exception {
// add your code here
        performSpeechRecognition();
        dispose();
    }

    private void onCancel() {
// add your code here if necessary
        try {
            stopSpeechRecognition();
        } catch (Exception e) {
            e.printStackTrace();
        }
        dispose();
    }

    public String performSpeechRecognition() throws Exception{

        Configuration cfg = new Configuration();
        cfg.setAcousticModelPath("resources\\acmodel\\");
        cfg.setDictionaryPath("resources\\mldic\\samsaaram.dic");
        cfg.setLanguageModelPath("resources\\lmmodel\\samsaaram.lm");
        LiveSpeechRecognizer recognizer = new LiveSpeechRecognizer(cfg);
        recognizer.startRecognition(true);
        SpeechResult result = recognizer.getResult();
        System.out.println(result.getHypothesis());
        showResult.setText("HI!!!");
        return result.getHypothesis();
    }

    public void stopSpeechRecognition() throws Exception{
        Configuration cfg = new Configuration();
        cfg.setAcousticModelPath("resources/acmodel/");
        cfg.setDictionaryPath("resources/mldic/samsaaram.dic");
        cfg.setLanguageModelPath("resources/lmmodel/samsaaram.lm.bin");
        LiveSpeechRecognizer recognizer = new LiveSpeechRecognizer(cfg);
        recognizer.stopRecognition();
        System.out.println("Recognition stopped!");
    }

    public static void main(String[] args) throws Exception{
        Samsaaram dialog = new Samsaaram();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
